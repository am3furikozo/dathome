#!/usr/bin/env bash

# run panel
tint2 &

# run user's apps
/usr/bin/emacs --daemon &
telegram-desktop -startintray &
discord --start-minimized &
