#!/usr/bin/env bash

# if DISPLAY is an empty string and the virtual terminal number is equal to one
if [ -z "${DISPLAY}" ] && [ "${XDG_VTNR}" -eq 1 ]; then
  exec startx ~/.xinitrc xmonad > /dev/null 2>&1
fi
